package com.brilgo.exemplo.api.java8;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class Exemplo5ApiJava8Streams {
	public static void main(String args[]) {
		List<String> strings = Arrays.asList("abc", "", "bc", "efg", "abcd", "", "jkl");
		List<String> filtered = null;
		String mergedString = null;
		List<Integer> numbers = Arrays.asList(3, 2, 2, 3, 7, 3, 5);
		List<Integer> squaresList = null;
		List<Integer> integers = Arrays.asList(1, 2, 13, 4, 15, 6, 17, 8, 19);
		List<BigDecimal> totais = Arrays.asList(BigDecimal.ONE, new BigDecimal(2010.1333), new BigDecimal(126613.01), new BigDecimal(227534.45), new BigDecimal(27000.02), new BigDecimal(15559.99), new BigDecimal(17889.89), new BigDecimal(31111.12));
		Random random = new Random();
		long count = 0;
		
		System.out.println("Valores na lista de strings: " + strings);
		count = strings.stream().filter(string -> string.isEmpty()).count();
		System.out.println("Quantidade de strings vazias: " + count);
		count = strings.stream().filter(string -> string.length() == 3).count();
		System.out.println("Quantidade de strings com tamanho igual a 3: " + count);

		filtered = strings.stream().filter(string -> string.startsWith("a")).collect(Collectors.toList());
		System.out.println("Filtra lista para itens que iniciam com a letra 'a': " + filtered);
		mergedString = strings.stream().filter(string -> !string.isEmpty()).collect(Collectors.joining("; "));
		System.out.println("Join strings com separador ';': " + mergedString);

		squaresList = numbers.stream().map(i -> i * i).distinct().collect(Collectors.toList());
		System.out.println("Calcula raiz quadrada da lista de numeros: " + squaresList);
		
		System.out.println("Lista de numeros inteiros: " + integers);
		IntSummaryStatistics stats = integers.stream().mapToInt(x -> x).summaryStatistics();
		System.out.println("Maior numero: " + stats.getMax());
		System.out.println("Menor numero: " + stats.getMin());
		System.out.println("Soma numeros: " + stats.getSum());
		System.out.println("Media numeros: " + stats.getAverage());
		
		System.out.println("Lista de totais: " + totais);
		Double somatorioTotais = totais.stream().mapToDouble(v -> v.doubleValue()).filter(v -> v > 200.50).sum();
		System.out.println("Soma numeros: " + new BigDecimal(somatorioTotais).setScale(2, RoundingMode.DOWN));
		
		List<Integer> randRes = random.ints().limit(10).boxed().sorted().collect(Collectors.toList());
		System.out.println("Imprime todos os números randomicos: " + randRes);
		count = strings.parallelStream().filter(string -> string.isEmpty()).count();
		System.out.println("Quantidade de strings vazias executando em paralelo: " + count);
	}
}
