package com.brilgo.exemplo.api.java8;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class Exemplo7ApiJava8JavaScriptEngine {
	
	public static void main(String args[]) {
		ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
		ScriptEngine nashorn = scriptEngineManager.getEngineByName("nashorn");

		String name = "Mahesh";
		Integer result = null;
		try {
			System.out.println("Script 1:");
			nashorn.eval("print('" + name + "')");
			
			System.out.println("Script 2:");
			result = (Integer) nashorn.eval("10 + 2");
			System.out.println(result.toString());
			
			System.out.println("Script 3 - sample.js:");
			Path path = Paths.get("sample.js");
			String script = new String(Files.readAllBytes(path));
			nashorn.eval(script);
		} catch (ScriptException | IOException e) {
			System.out.println("Error executing script: " + e.getMessage());
			e.printStackTrace();
		}
	}
}
