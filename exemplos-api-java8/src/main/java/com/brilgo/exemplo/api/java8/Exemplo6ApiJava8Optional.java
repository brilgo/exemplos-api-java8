package com.brilgo.exemplo.api.java8;

import java.util.Optional;

public class Exemplo6ApiJava8Optional {
	
	public static final Integer INTEGER_DEFAULT_VALUE = new Integer(0);
	
	public static void main(String args[]) {
		Exemplo6ApiJava8Optional exemplo = new Exemplo6ApiJava8Optional();
		Integer value1 = null;
		Integer value2 = new Integer(10);

		Optional<Integer> a = Optional.ofNullable(value1);
		Optional<Integer> b = Optional.of(value2);
		System.out.println(exemplo.sum(a, b));
	}

	public Integer sum(Optional<Integer> a, Optional<Integer> b) {
		System.out.println("First parameter is present: " + a.isPresent());
		System.out.println("Second parameter is present: " + b.isPresent());
		return a.orElse(INTEGER_DEFAULT_VALUE) + b.orElse(INTEGER_DEFAULT_VALUE);
	}
}
