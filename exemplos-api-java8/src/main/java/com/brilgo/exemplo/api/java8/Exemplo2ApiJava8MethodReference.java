package com.brilgo.exemplo.api.java8;

import java.util.ArrayList;
import java.util.List;

public class Exemplo2ApiJava8MethodReference {
	
	static final Double FATOR = 2.3d;
	
	public static void main(String args[]) {
		List<String> names = new ArrayList<>();
		names.add("Mahesh");
		names.add("Suresh");
		names.add("Ramesh");
		names.add("Naresh");
		names.add("Kalpesh");
		names.forEach(System.out::println);
		
		List<Double> numbers = new ArrayList<>();
		numbers.add(2d);
		numbers.add(4d);
		numbers.add(12.5);
		numbers.add(18d);
		numbers.add(21.8d);
		numbers.forEach(Exemplo2ApiJava8MethodReference::multiplicaFatorEImprime);
	}
	
	public static void multiplicaFatorEImprime(Double valor) {
		System.out.println(valor * FATOR);
	}
	
	public static Double multiplicaFator(Double valor) {
		return valor * FATOR;
	}
}
