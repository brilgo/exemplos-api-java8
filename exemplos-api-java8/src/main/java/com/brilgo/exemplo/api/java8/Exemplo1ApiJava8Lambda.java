package com.brilgo.exemplo.api.java8;

public class Exemplo1ApiJava8Lambda {

	final static String HELLO = "Hello!";
	
	public static void main(String[] args) {
		Exemplo1ApiJava8Lambda exemplo = new Exemplo1ApiJava8Lambda();
		// with type declaration
		MathOperation addition = (int a, int b) -> a + b;
		// with out type declaration
		MathOperation subtraction = (a, b) -> a - b;
		// with return statement along with curly braces
		MathOperation multiplication = (int a, int b) -> {
			return a * b;
		};
		// without return statement and without curly braces
		MathOperation division = (int a, int b) -> a / b;
		MathOperation potentiation = (a, b) -> (int) Math.pow( Integer.valueOf(a).doubleValue(), Integer.valueOf(b).doubleValue() );
		System.out.println("10 + 5 = " + exemplo.operate(10, 5, (a, b) -> a + b));
		System.out.println("10 + 5 = " + exemplo.operate(10, 5, addition));
		System.out.println("10 - 5 = " + exemplo.operate(10, 5, subtraction));
		System.out.println("10 x 5 = " + exemplo.operate(10, 5, multiplication));
		System.out.println("10 / 5 = " + exemplo.operate(10, 5, division));
		System.out.println("10 ^ 5 = " + exemplo.operate(10, 5, potentiation));

		// without parenthesis
		GreetingService greetService1 = message -> System.out.println("Hello " + message);
		// with parenthesis
		GreetingService greetService2 = (message) -> System.out.println("Hello " + message);
		final String olaLocalVar = "Ola";
		GreetingService greetService3 = message -> {System.out.print(HELLO + " L1 " + message + " ");System.out.println(olaLocalVar + " L2 " + message);};
		
		greetService1.sayMessage("Mahesh");
		greetService2.sayMessage("Suresh");
		greetService3.sayMessage("John");
	}

	interface MathOperation {
		int operation(int a, int b);
	}

	interface GreetingService {
		void sayMessage(String message);
	}

	private int operate(int a, int b, MathOperation mathOperation) {
		return mathOperation.operation(a, b);
	}
}
